# ![DoYourGroceries Logo](assets/appLogo/DoYourGroceriesLogo.png) DoYourGroceries

**DoYourGroceries** este o aplicație mobilă cross-platform dezvoltată cu framework-ul Flutter, destinată planificării cumpărăturilor de produse alimentare.

## Precondiții

Pentru a instala Flutter, urmați pașii specifici sistemului dumneavoastră de operare:
- [Windows](https://docs.flutter.dev/get-started/install/windows/mobile)
- [macOS](https://docs.flutter.dev/get-started/install/macos/mobile-ios)
- [Linux](https://docs.flutter.dev/get-started/install/linux/android)

## Instalare și Rulare
Utilizatorul trebuie să folosească următoarele comenzi pentru a porni aplicația:

1. Clonați acest repository:

    ```bash
    git clone https://github.com/utilizator/do_your_groceries.git
    cd do_your_groceries
    ```

2. Instalați pachetele necesare:

    ```bash
    flutter pub get
    ```

3. Porniți aplicația pe un emulator sau dispozitiv fizic:

    ```bash
    flutter run
    ```
