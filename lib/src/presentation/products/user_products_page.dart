import 'package:do_you_groceries/src/actions/index.dart';
import 'package:do_you_groceries/src/containers/pending_container.dart';
import 'package:do_you_groceries/src/containers/selected_list_container.dart';
import 'package:do_you_groceries/src/models/index.dart';
import 'package:do_you_groceries/src/ui_elements/components/background_wave_clipper.dart';
import 'package:do_you_groceries/src/ui_elements/components/hero_posts_widget.dart';
import 'package:do_you_groceries/src/ui_elements/components/home_page_components/bottom_app_bar.dart';
import 'package:do_you_groceries/src/ui_elements/components/shimmer/shimmer_products.dart';
import 'package:do_you_groceries/src/ui_elements/components/sliver_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:redux/redux.dart';

class UserProductsPage extends StatefulWidget {
  const UserProductsPage({super.key});

  @override
  State<UserProductsPage> createState() => _UserProductsPageState();
}

class _UserProductsPageState extends State<UserProductsPage> {
  late Store<AppState> _store;
  final ValueNotifier<double> totalPriceNotifier = ValueNotifier<double>(0);
  int totalPriceLength = 0;

  @override
  void initState() {
    super.initState();

    _store = StoreProvider.of<AppState>(context, listen: false);
    _store.dispatch(ListenForProductsStart(_store.state.selectedGroceryList!.groceryListId));
  }

  @override
  void dispose() {
    _store
      ..dispatch(ListenForProductsDone(_store.state.selectedGroceryList!.groceryListId))
      ..dispatch(const SetUnselectedList());

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SelectedListContainer(
      builder: (BuildContext context, GroceryList groceryList) {
        double totalPrice = 0;
        for (final Product product in _store.state.productsGroceryList) {
          totalPrice += product.price;
        }
        totalPriceNotifier.value = totalPrice;
        return Scaffold(
          body: Stack(
            children: <Widget>[
              PendingContainer(
                builder: (BuildContext context, Set<String> pending) {
                  if ((pending.contains(ListenForProducts.pendingKey)) ||
                      (pending.contains(SwitchProduct.pendingKey)) ||
                      (pending.contains(CreateProduct.pendingKey)) ||
                      (pending.contains(AddProductToGroceryList.pendingKey)) ||
                      (pending.contains(SmartUpdateList.pendingKey))) {
                    return Column(
                      children: <Widget>[
                        BackgroundWave(
                          pageName: _store.state.selectedGroceryList!.title,
                          iconWidget: SvgPicture.asset(
                            'assets/groceryListIcons/${_store.state.selectedGroceryList!.selectedIcon}.svg',
                            height: 100,
                            width: 100,
                          ),
                        ),
                        const ShimmerProducts(),
                      ],
                    );
                  }
                  return CustomScrollView(
                    slivers: <Widget>[
                      SliverPersistentHeader(
                        delegate: SliverAppBarProducts(
                          _store.state.selectedGroceryList!.selectedIcon,
                          _store.state.selectedGroceryList!.title,
                        ),
                        pinned: true,
                      ),
                      if (_store.state.productsGroceryList.isNotEmpty)
                        SliverList(
                          delegate: SliverChildBuilderDelegate(
                            childCount: _store.state.productsGroceryList.length,
                            (BuildContext context, int index) {
                              return HeroPosts(
                                product: _store.state.productsGroceryList[index],
                              );
                            },
                          ),
                        )
                      else
                        SliverList(
                          delegate: SliverChildBuilderDelegate(
                            childCount: 1,
                            (BuildContext context, int index) {
                              return const Center(child: Text('Nu ai produse momentan.\nTe rog adaugă câteva!'));
                            },
                          ),
                        ),
                    ],
                  );
                },
              ),
              Positioned(
                bottom: MediaQuery.of(context).size.height * 0.01,
                right: MediaQuery.of(context).size.width * 0.67 - totalPrice.toStringAsFixed(2).length * 2,
                child: ValueListenableBuilder<double>(
                  valueListenable: totalPriceNotifier,
                  builder: (BuildContext context, double totalPrice, Widget? child) {
                    return Container(
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        color: Colors.lightBlue,
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: Text(
                        'Total ${totalPrice.toStringAsFixed(2)} RON',
                        style: const TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              _store.dispatch(const GetCamerasStart());
              context.pushNamed('cameraApp');
            },
            tooltip: 'Camera',
            child: const Icon(Icons.camera_alt),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          bottomNavigationBar: BottomAppBarWidget(
            store: _store,
          ),
        );
      },
    );
  }
}
